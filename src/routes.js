import {
  createAppContainer,
  createStackNavigator,
  createBottomTabNavigator,
} from 'react-navigation';

import Main from '~/pages/Main';
import Login from '~/pages/Login';
import BankSlips from '~/pages/BankSlips';
import MyBankSlips from '~/pages/MyBankSlips';
import Configs from '~/pages/Configs';
import Notifications from '~/pages/Notifications';

const Routes = createAppContainer(
  createStackNavigator({
    Login,
    User: createBottomTabNavigator({
      BankSlips,
      MyBankSlips,
      Configs,
      Notifications,
    }),
  }),
);

export default Routes;
