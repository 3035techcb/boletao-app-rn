import React from 'react';

import {
  Container, Form, InputCpf, LoginButton, LoginButtonText,
} from './styles';

export default function Login({ navigation }) {
  return (
    <Container>
      <Form>
        <InputCpf />
        <LoginButton onPress={() => navigation.navigate('User')}>
          <LoginButtonText>ENTRAR</LoginButtonText>
        </LoginButton>
      </Form>
    </Container>
  );
}
