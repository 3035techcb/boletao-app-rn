import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: #fff;
`;

export const Form = styled.View`
  flex-direction: column;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const InputCpf = styled.TextInput.attrs({
  placeholder: 'INSIRA SEU CPF',
  autoCapitalize: 'none',
  autoCorrect: false,
  keyboardType: 'numeric',
})`
  border: 1px solid #fff;
  height: 50px;
  border-radius: 15px;
  width: 80%;
  text-align: center;
  padding: 0 20px;
  background-color: #fff;
  box-shadow: 1px 1px 10px lightgrey;
  color: darkgrey;
`;

export const LoginButton = styled.TouchableOpacity.attrs({
  activeOpacity: 0.6,
})`
  width: 80%;
  background-color: #333;
  border: 1px solid #333;
  border-radius: 10px;
  height: 50px;
  margin-top: 20px;
  align-items: center;
  justify-content: center;
`;

export const LoginButtonText = styled.Text`
  color: #fff;
  text-align: center;
  font-size: 16px;
`;
